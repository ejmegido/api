var express = require('express')
var app = express()
var port = process.env.PORT || 3000
var fs = require('fs')
var bodyParser = require('body-parser')
app.use(bodyParser.json())
var requestJson = require('request-json')
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/mapicas/collections"
var apiKey = "apiKey=v5i8YUL_WBpc0qQHsjhSPAPysyxNIyLT"
var clienteMlab = null
var speakeasy = require('speakeasy')

app.listen(port)

console.log("API escuchando en el puerto " + port)

app.get('/apitechu/v3/createMongoDB', function(req, res)
{

  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://localhost:27017/emilio";

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    console.log("Database created!");
    db.close();
    });
})

app.post('/apitechu/v3/adduser', function(req, res){
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://admin:admin123@localhost:27017/admin";
  var email = req.headers.email
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("admin");
    dbo.collection("usuarios").find({email: email }).toArray(function(err, result) {
      if (err) throw err;
      if (result.length == 0) {
        dbo.collection("configuracion").findOne({}, function(err, resultb) {
          if (err) throw err;
          var contador = resultb.contador
          var password_cifrado = resultb.password_cifrado
          var algorithm = resultb.algoritmo_cifrado
          console.log(password_cifrado)
          console.log(algorithm)
          var myquery = { contador: contador }
          var nuevocon = contador + 1
          var nuevaconf = { $set: {contador: nuevocon} }
          var crypto = require('crypto')

          function encrypt(text){
            var cipher = crypto.createCipher(algorithm,password_cifrado)
            var crypted = cipher.update(text,'utf8','hex')
            crypted += cipher.final('hex');
            return crypted;
          }

          function decrypt(text){
            var decipher = crypto.createDecipher(algorithm,password_cifrado)
            var dec = decipher.update(text,'hex','utf8')
            dec += decipher.final('utf8');
            return dec;
          }


          dbo.collection("configuracion").updateOne(myquery, nuevaconf, function(err, obj) {
            if (err) throw err;
          })
          var pass = encrypt(req.headers.password)
          //Obtención del secreto para transferencias
          var secret = speakeasy.generateSecret({length: 20});
          var tempsecret = secret.base32
          //Construimos el document para la colección usuarios
          var nuevo = {"id": nuevocon, "first_name": req.headers.first_name, "last_name": req.headers.last_name, "email": req.headers.email, "gender": req.headers.gender, "ip_address": req.headers.ip_address, "country": req.headers.country, "telefono": parseInt(req.headers.telefono), "password": pass, "secret": tempsecret }
          dbo.collection("usuarios").insertOne(nuevo, function(err, resp) {
            if (err) throw err;
            res.send(nuevo)
            db.close()
          })
        })
      }
      else {
        res.send("Usuario con email " + email + " ya existe")
        db.close()
      }
    })
  })
})

app.post('/apitechu/v3/deluser', function(req, res)
{
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://admin:admin123@localhost:27017/admin";
  var id = parseInt(req.headers.id)
  MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("admin");
  var myquery = { id: id };
  dbo.collection("usuarios").deleteOne(myquery, function(err, obj) {
    if (err) throw err;
    db.close();
  });
});
res.send("Usuario " + req.headers.id + " eliminado")
})

app.post('/apitechu/v3/deluseremail', function(req, res)
{
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://admin:admin123@localhost:27017/admin";
  var email = req.headers.email
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("admin");
    var myquery = { email: email };
    dbo.collection("usuarios").deleteOne(myquery, function(err, obj) {
      if (err) throw err;
      console.log(obj.result.n)
      if (obj.result.n == 1)
      {
        res.send("Usuario " + req.headers.email + " eliminado")
      }
      else
      {
        res.send("Usuario " + req.headers.email + " no existe")
      }
      db.close()
    })
  })
})

app.post('/apitechu/v3/moduser', function(req, res)
{
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://admin:admin123@localhost:27017/admin";
  var id = parseInt(req.headers.id)
  var myquery = { id: id };
  var newvalues = { $set: {first_name: req.headers.first_name, last_name: req.headers.last_name } };
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("admin");
    dbo.collection("usuarios").find(myquery).toArray(function(err, result) {
      if (err) throw err;
      var resultado = result.length
      if (result.length == 0) {
        res.send("Usuario " + id + ", " + req.headers.first_name + " " + req.headers.last_name + " no existe!!")
        db.close()
      }
      else {
          dbo.collection("usuarios").updateOne(myquery, newvalues, function(err, obj) {
          if (err) throw err;
          db.close()
          res.send("Usuario " + id + ", " + req.headers.first_name + " " + req.headers.last_name + " actualizado!!")
        })
      }
    })
  })
})

//Devuelve la lista de usuarios
app.get('/apitechu/v3/usuarios', function(req,res)
{
  var MongoClient = require('mongodb').MongoClient;
  //var url = "mongodb://localhost:27017"
  var url = "mongodb://admin:admin123@localhost:27017/admin";
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("admin");
    dbo.collection("usuarios").find({},{_id: 0, first_name: 1}).toArray(function(err, result) {
      if (err) throw err;
      res.send(JSON.stringify(result))
      db.close();
    });
  });
})

//Crear cuenta en MongoDB (Headers)
app.post('/apitechu/v3/addcuenta', function(req, res)
{
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://admin:admin123@localhost:27017/admin";
  var partea = Math.floor(Math.random()*89999999999)+10000000000
  var parteb = Math.floor(Math.random()*89999999999)+10000000000
  var iban = ("ES" + partea + parteb)
  var date = new Date()
  var year = date.getFullYear()
  var day  = date.getDate();
  var month = date.getMonth() + 1
  var hour = date.getHours()
  var minutes = date.getMinutes()
  var seconds = date.getSeconds()
  var fecha = (hour + ":" + minutes + ":" + seconds + " " + day + "/" + month + "/" + year)
  var movimientos = []
  movimientos = [{"id": 1,"fecha": fecha,"importe": 0,"moneda": "EUR"}]
  var nuevo = {"iban": iban, "idcliente": parseInt(req.headers.idcliente), "movimientos": movimientos, "saldo": 0 }
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("admin");
    dbo.collection("cuentas").insertOne(nuevo, function(err, res) {
      if (err) throw err;
      db.close();
    });
  });
  res.send({"Cuenta": nuevo})
})

//MONGODB - Posicion global dado id cliente (headers)
app.get('/apitechu/v3/cuentas/posglobal', function(req, res)
{
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://admin:admin123@localhost:27017/admin";
  var id = parseInt(req.headers.idcliente)
  MongoClient.connect(url, function(err, db)
  {
    if (err) throw err;
    var dbo = db.db("admin");
    var myquery = { idcliente: id }
    dbo.collection("cuentas").find({idcliente: id }).toArray(function(err, result)
    {
      if (err) throw err;
      var saldo = 0
      for (var i =0; i < result.length; i++)
      {
        saldo = saldo + result[i].saldo
      }
      res.send({saldo})
      db.close();
    });
  });
})

app.get('/apitechu/v3/cuentas/:id', function(req, res)
{
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://admin:admin123@localhost:27017/admin";
  var id = parseInt(req.params.id)
  console.log("ID:" + id)
  MongoClient.connect(url, function(err, db)
  {
    if (err) throw err;
    var dbo = db.db("admin");
    var myquery = { idcliente: id }
    console.log("Busqueda:" + myquery)
    dbo.collection("cuentas").find({idcliente: id }).toArray(function(err, result)
    {
      if (err) throw err;
      res.send(JSON.stringify(result))
      console.log("Elementos en resultado:" + result.length)
      db.close();
    });
  });
})

app.get('/apitechu/v3/cuentas/', function(req, res)
{
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://admin:admin123@localhost:27017/admin";
  var id = parseInt(req.headers.idcliente)
  MongoClient.connect(url, function(err, db)
  {
    if (err) throw err;
    var dbo = db.db("admin");
    var myquery = { idcliente: id }
    console.log("Busqueda:" + myquery)
    dbo.collection("cuentas").find({idcliente: id }).toArray(function(err, result)
    {
      if (err) throw err;
      res.send(JSON.stringify(result))
      console.log("Elementos en resultado:" + result.length)
      db.close();
    });
  });
})

//MONGODB - Login de usuario con mail+password por cabecera
app.post('/apitechu/v3/login', function(req, res)
{
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://admin:admin123@localhost:27017/admin"
  var email = req.headers.email
  var objeto = []
  MongoClient.connect(url, function(err, db)
  {
    var dbo = db.db("admin");
    dbo.collection("configuracion").findOne({}, function(err, resultb) {
      if (err) throw err;
      var password_cifrado = resultb.password_cifrado
      var algorithm = resultb.algoritmo_cifrado
      var crypto = require('crypto')

      function encrypt(text){
        var cipher = crypto.createCipher(algorithm,password_cifrado)
        var crypted = cipher.update(text,'utf8','hex')
        crypted += cipher.final('hex');
        return crypted;
      }

      function decrypt(text){
        var decipher = crypto.createDecipher(algorithm,password_cifrado)
        var dec = decipher.update(text,'hex','utf8')
        dec += decipher.final('utf8');
        return dec;
      }
    if (err) throw err;
    var password = encrypt(req.headers.password)
    var myquery = { email: email, password: password }
    dbo.collection("usuarios").find(myquery).toArray(function(err, result)
    {
      if (err) throw err;
      console.log(myquery)
      console.log(result.length)
      if (result.length == 1)
      {
        var cambio = { $set: {logged: true } }
        dbo.collection("usuarios").updateOne(myquery, cambio, function(err, obj)
        {
          if (err) throw err;
          objeto = JSON.parse(obj)
          db.close()
          res.send({"Login": "OK","id":result[0].id,"Nombre":result[0].first_name,"Apellidos":result[0].last_name})
        })
      }
      else
      {
        res.send({"Login": "KO"})
      }
    })
    })
  })
})

//MONGODB - Logout de usuario con id por cabecera
app.post('/apitechu/v3/logout', function(req, res)
{
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://admin:admin123@localhost:27017/admin"
  var id = parseInt(req.headers.id)
  MongoClient.connect(url, function(err, db)
  {
    if (err) throw err;
    var dbo = db.db("admin");
    var myquery = { id: id}
    dbo.collection("usuarios").find(myquery).toArray(function(err, result)
    {
      if (err) throw err;
      if (result.length == 1)
      {
        var cambio = { $set: {logged: false } }
        dbo.collection("usuarios").updateOne(myquery, cambio, function(err, obj)
        {
          if (err) throw err;
          console.log("Usuario ha salido")
          db.close()
          res.send({"Logout": "OK","id":result[0].id,"Nombre":result[0].first_name,"Apellidos":result[0].last_name})
        })
      }
      else
      {
        res.send("Usuario con id " + id + " no encontrado")
      }
    });
  });
})

//MONGODB - Movimientos a partir de IBAN por PARAM
app.get('/apitechu/v3/movimientos/:iban', function(req, res)
{
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://admin:admin123@localhost:27017/admin";
  var iban = req.params.iban
  var movimientos = []
  MongoClient.connect(url, function(err, db)
  {
    if (err) throw err;
    var dbo = db.db("admin");
    var myquery = { iban: iban }
    dbo.collection("cuentas").find(myquery, { _id: 0, movimientos: 1 }).toArray(function(err, result)
    {
      if (err) throw err;
      movimientos = JSON.stringify(result[0].movimientos)
      res.send(movimientos)
      db.close();
    });
  });
})

//MONGODB - Movimientos a partir de IBAN por HEADERS
app.get('/apitechu/v3/movimientos', function(req, res)
{
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://admin:admin123@localhost:27017/admin";
  var iban = req.headers.iban
  var movimientos = []
  MongoClient.connect(url, function(err, db)
  {
    if (err) throw err;
    var dbo = db.db("admin");
    var myquery = { iban: iban }
    dbo.collection("cuentas").find(myquery, { _id: 0, movimientos: 1 }).toArray(function(err, result)
    {
      if (err) throw err;
      if (result.length >= 1)
      {
        movimientos = JSON.stringify(result[0].movimientos)
        res.send(movimientos)
        db.close()
      }
      else
      {
        res.send("IBAN no encontrado")
        db.close()
      }
    })
  })
})

app.get('/apitechu/v3/ibanporid', function(req, res)
{
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://admin:admin123@localhost:27017/admin";
  var id = parseInt(req.headers.idcliente)
  var myquery = { idcliente: id }
  var iban = []
  MongoClient.connect(url, function(err, db)
  {
    if (err) throw err;
    var dbo = db.db("admin");
    dbo.collection("cuentas").find(myquery, {"iban":1, "_id":0}).toArray(function(err, result)
    {
      if (err) throw err;
      if (result.length >= 1){
        var iban = result[0].iban
        res.send(result)
        db.close();
      }
      else {
        res.send("El usuario " + req.headers.idcliente + " no tiene cuentas")
      }
    })
  })
})

app.post('/apitechu/v3/nuevomov', function(req, res)
{
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://admin:admin123@localhost:27017/admin";
  var iban = req.headers.iban
  var importe = parseInt(req.headers.importe)
  var moneda = req.headers.moneda
  movimientos = []
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("admin")
    var myquery = { iban: iban }
    dbo.collection("cuentas").find(myquery).toArray(function(err, result)
    {
      if (err) throw err;
      if (result.length == 1)
      {
        movimientos = result[0].movimientos
        var date = new Date()
        var year = date.getFullYear()
        var day  = date.getDate();
        var month = date.getMonth() + 1
        var minutes = date.getMinutes()
        var seconds = date.getSeconds()
        var fecha = (hour + ":" + minutes + " " + day + "/" + month + "/" + year)
        nuevo = {"id": result[0].movimientos.length + 1, "fecha": fecha, "importe": importe, "moneda": moneda }
        movimientos.push(nuevo)
        var saldo = result[0].saldo
        var nuevosaldo = saldo + importe
        var newvalues = { $set: {movimientos: movimientos, saldo: nuevosaldo } };
        dbo.collection("cuentas").updateOne(myquery, newvalues, function(err, res) {
          if (err) throw err;
          db.close()
        })
        res.send("Movimientos en la cuenta con IBAN:" + iban + " -->" + JSON.stringify(movimientos) + "--- Saldo inicial:" + saldo + "---Importe:" + importe + "---Nuevo saldo:" + nuevosaldo)
      }
      else
      {
        res.send("IBAN " + iban + " no encontrado.")
      }
    });
  });
})

app.post('/apitechu/v3/transferencia', function(req, res)
{
  var MongoClient = require('mongodb').MongoClient;
  var MongoClient2 = require('mongodb').MongoClient;
  var url = "mongodb://admin:admin123@localhost:27017/admin";
  var ibanorig = req.headers.ibanorig
  var ibandes = req.headers.ibandes
  var importe = parseInt(req.headers.importe)
  var importeneg = 0 - parseInt(req.headers.importe)
  var moneda = req.headers.moneda
  movimientos = []
  var date = new Date()
  var year = date.getFullYear()
  var day  = date.getDate();
  var month = date.getMonth() + 1
  var hour = date.getHours()
  var minutes = date.getMinutes()
  var seconds = date.getSeconds()
  var fecha = (hour + ":" + minutes + ":" + seconds + " " + day + "/" + month + "/" + year)
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("admin")
    var cuentaorig = { iban: ibanorig }
    dbo.collection("cuentas").find(cuentaorig).toArray(function(err, result)
    {
      if (err) throw err;
      if (result.length == 1)
      {
        movimientos = result[0].movimientos
        nuevo = {"id": result[0].movimientos.length + 1, "fecha": fecha, "importe": importeneg, "moneda": moneda }
        movimientos.push(nuevo)
        var saldo = result[0].saldo
        var nuevosaldo = saldo + importeneg
        var newvalues = { $set: {movimientos: movimientos, saldo: nuevosaldo } };
        dbo.collection("cuentas").updateOne(cuentaorig, newvalues, function(err, res) {
          if (err) throw err;
          db.close()
        })
      }
    })
  })
  MongoClient2.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("admin")
    var cuentades = { iban: ibandes }
    dbo.collection("cuentas").find(cuentades).toArray(function(err, result)
    {
      if (err) throw err;
      if (result.length == 1)
      {
        movimientos = result[0].movimientos
        nuevo = {"id": result[0].movimientos.length + 1, "fecha": fecha, "importe": importe, "moneda": moneda }
        movimientos.push(nuevo)
        var saldo = result[0].saldo
        var nuevosaldo = saldo + importe
        var newvalues = { $set: {movimientos: movimientos, saldo: nuevosaldo } };
        dbo.collection("cuentas").updateOne(cuentades, newvalues, function(err, res) {
          if (err) throw err;
          db.close()
        })
        res.send("Origen:" + ibanorig + "-----Destino:" + ibandes)
      }
      else
      {
        res.send("IBAN " + ibanorig + " no encontrado.")
      }
    })
  })
})


app.post('/apitechu/v3/motivomail', function(req, res)
{
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://admin:admin123@localhost:27017/admin";
  var nuevo = {"email": req.headers.email, "motivo": req.headers.motivo}
  console.log(nuevo)
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("admin");
    dbo.collection("bajas").insertOne(nuevo, function(err, res) {
      if (err) throw err;
      console.log("Baja informada en colección baja: " + nuevo);
      db.close();
    });
  });
  res.send({"Baja": nuevo})
})

app.get('/apitechu/v3/getsecretid', function(req, res){
  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://admin:admin123@localhost:27017/admin";
  var id = parseInt(req.headers.id)
  MongoClient.connect(url, function(err, db)
  {
    if (err) throw err;
    var dbo = db.db("admin");
    var myquery = { id: id }
    dbo.collection("usuarios").find({id: id }).toArray(function(err, result)
    {
      if (err) throw err;
      if (result.length >= 1)
      {
        secret = result[0].secret
        res.send({secret})
      }
      else
      {
          res.send("Usuario " + id + " no encontrado.")
      }
    })
  })
})

app.get('/apitechu/v3/2f', function(req, res){

  var tempsecret = req.headers.secret.toString()
  var userToken = req.headers.token.toString()

    mitoken = speakeasy.totp({
       secret: tempsecret,
       encoding: 'base32'
       });

    var window = 0

    var tokenValidates = speakeasy.totp.verify({
                secret: tempsecret,
                encoding: 'base32',
                token: userToken,
                window: 0
                });

    console.log(tokenValidates + " Mi secreto: " + tempsecret + " Mitoken: " + mitoken)
    res.send({"Resultado":tokenValidates})
})
